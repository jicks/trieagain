ARCH=PROC_X86
ifeq ($(ARCH), PROC_X64)
PROCFLAGS=-m64
else
PROCFLAGS=-m32
endif
CXXFLAGS= ${PROCFLAGS} -D ${ARCH} -std=c++11 -O3
CXX=g++
CC=gcc

EXECUTABLEBUILD_NAME=TextMiningCompiler
EXECUTABLERUN_NAME=TextMiningApp

SRCBUILD=\
	Trie.cc\
	Emit.cc\
	Main.cc

SRCRUN=\
	Loader.cc\
	Storage.cc\
	Trie.cc\
	Main.cc

#GENERATE THE FILENAME FOR MAKE
SRCBUILDPREFIXED=$(addprefix src/build/, $(SRCBUILD))
SRCRUNPREFIXED=$(addprefix src/run/, $(SRCRUN))
SRCBUILDCOMPILED__0=$(SRCBUILDPREFIXED)
SRCRUNCOMPILED__0=$(SRCRUNPREFIXED)
SRCBUILDCOMPILED__1=$(SRCBUILDCOMPILED__0:.c=.o)
SRCRUNCOMPILED__1=$(SRCRUNCOMPILED__0:.c=.o)
SRCBUILDCOMPILED=$(SRCBUILDCOMPILED__1:.cc=.o)
SRCRUNCOMPILED=$(SRCRUNCOMPILED__1:.cc=.o)

all: build run

build: $(EXECUTABLEBUILD_NAME)
run: $(EXECUTABLERUN_NAME)
$(EXECUTABLEBUILD_NAME): $(SRCBUILDCOMPILED)
	$(CXX) $(CXXFLAGS) $^ -o $(EXECUTABLEBUILD_NAME)
$(EXECUTABLERUN_NAME):  $(SRCRUNCOMPILED)
	$(CXX) $(CXXFLAGS) $^ -o $(EXECUTABLERUN_NAME)
%.o:%.cc
	$(CXX) $(CXXFLAGS) -c $^ -I src/tools -o $@
%.o:%.c
	$(CC) $(CFLAGS) -c $^ -I src/tools -o $@
clean:
	rm -rf $(SRCBUILDCOMPILED) $(SRCRUNCOMPILED) $(EXECUTABLEBUILD_NAME) $(EXECUTABLERUN_NAME)
	rm -rf ./*.bin ./src/asm/*.bin
	$(MAKE) -C documentation clean
check: all
	mkdir -p check/my
	mkdir -p check/ref
	cp $(EXECUTABLEBUILD_NAME) check/my/
	cp $(EXECUTABLERUN_NAME) check/my/
	chmod +x check/run.sh
	./check/run.sh
doc:
	mkdir -p documentation/
	$(MAKE) -C documentation all
