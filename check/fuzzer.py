#! /usr/bin/env python2

import sys, os, random, string

def launchTest(cmd):
  ret = True
  cmdname = cmd.strip().replace(' ', '_') + "." +  str(os.getpid())
  os.system('echo "%s" | ./my/TextMiningApp ./my/words.bin > "%s.my" 2>/dev/null' % (cmd, cmdname))
  os.system('echo "%s" | ./ref/TextMiningApp ./ref/words.bin > "%s.ref" 2>/dev/null' % (cmd, cmdname))
  if os.system('diff "%s.ref" "%s.my" > /dev/null' % (cmdname, cmdname)):
    ret = False
  os.unlink('%s.ref' % cmdname)
  os.unlink('%s.my' % cmdname)
  return ret

def randomWord(words):
  index = random.randint(0, len(words) - 1)
  distance = int(random.expovariate(1.5))
  while distance > 4:
      distance = int(random.expovariate(1.5))
  word = words[index]
  if random.randint(1, 10) == 1:
    pos = random.randint(0, len(word) - 1)
    c = random.choice(string.ascii_letters + string.digits)
    word = word[:pos] + c + word[pos+1:]
    if random.randint(0, 1) == 0:
      pos = random.randint(0, len(word) - 1)
      c = random.choice(string.ascii_letters + string.digits)
      word = word[:pos] + c + word[pos+1:]
  return (word, distance)

if len(sys.argv) != 2:
  print 'Usage: %s <number of test>' % sys.argv[0]
  sys.exit(1)

numberOfTest = int(sys.argv[1])
fails = 0
currentFails = 0
words = []

print '[Fuzzer] Generating words list... ',

with open('words.txt', 'r') as f:
  for line in f:
    words.append(line.split()[0])

print 'Done'


checklist = open('check.list', 'a+')
faillist = open('fail.list', 'a+')

for i in xrange(numberOfTest):
  word, distance = randomWord(words)
  cmd = "approx %d %s\n" % (distance, word)
  checklist.write(cmd)
  if not launchTest(cmd):
    faillist.write(cmd)
    currentFails += 1
    fails += 1
  if numberOfTest < 100 or not i % (numberOfTest / 100):
    failstring = "\033[32mPASS\033[0m"
    if currentFails > 0:
      failstring = "\033[33m%d NEW FAIL\033[0m" % currentFails
    currentFails = 0
    print '[Fuzzer] %2d%%  => ' % (i * 100 / numberOfTest), failstring

failstring = "\033[32m0 FAIL\033[0m"
if fails > 0:
  failstring = "\033[33m%d FAIL\033[0m" % fails
print '[Fuzzer] Total  => ', failstring

checklist.close()
faillist.close()
