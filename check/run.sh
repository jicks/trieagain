#! /bin/sh
cd check
rm -rf tmp_*.tmp

if test -f ref/TextMiningCompiler
then
    echo "[Check] ref/TextMiningCompiler is present"
else
    echo "[Check] downloading ref/TextMiningCompiler"
    wget "http://www.rednet.fr/epita/projet/ref/linux64/TextMiningCompiler"
    mv TextMiningCompiler ref/TextMiningCompiler
fi



if test -f ref/TextMiningApp
then
    echo "[Check] ref/TextMiningApp is present"
else
    echo "[Check] downloading ref/TextMiningApp"
    wget "http://www.rednet.fr/epita/projet/ref/linux64/TextMiningApp"
    mv TextMiningApp ref/TextMiningApp
fi

if test -f words.txt
then
    echo "[Check] words.txt is present"
else
    echo "[Check] downloading words.txt"
    wget "http://www.rednet.fr/epita/projet/words.txt"
fi

rm -rf my/words.bin
rm -rf ref/words.bin
echo "[Test] Generating dictionary"
chmod +r words.txt
chmod +x ./my/TextMiningCompiler
chmod +x ./my/TextMiningApp
chmod +x ./ref/TextMiningCompiler
chmod +x ./ref/TextMiningApp

./my/TextMiningCompiler words.txt ./my/words.bin
./ref/TextMiningCompiler words.txt ./ref/words.bin

if test -f ref/words.bin
then
    echo -e "\033[32m[Passed]\033[0m Ref has created dictionary"
else
    echo -e "\033[31m[Failed]\033[0m Ref hasn't created dictionary"
    exit 1
fi

if test -f my/words.bin
then
    echo -e "\033[32m[Passed]\033[0m My has created dictionary"
else
    echo -e "\033[32m[Failed]\033[0m My hasn't created dictionary"
    exit 1
fi

passed=0
failed=0
echo "[test] Start global test suite"
for file in tests/*
do
    cat $file | ./my/TextMiningApp ./my/words.bin | tr '}' '\012' > tmp_myoutput.tmp
    cat $file | ./ref/TextMiningApp ./ref/words.bin | tr '}' '\012' > tmp_refoutput.tmp
    diff tmp_myoutput.tmp tmp_refoutput.tmp > /dev/null
    if test $? -eq 0
    then
	echo -e "\033[32m[Passed]\033[0m" $file
	passed=$(($passed + 1))
    else
	echo -e "\033[31m[Failed]\033[0m" $file
	failed=$(($failed + 1))
	diff -y tmp_myoutput.tmp tmp_refoutput.tmp
    fi
    rm -rf tmp_*.tmp
done

echo "[result]" "passed =" $passed "failed =" $failed 
