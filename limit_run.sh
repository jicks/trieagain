#! /bin/sh

if [ $# -lt 2 ]; then
    echo "Usage: $0 <memory_limit> <command> [arguments]
The memory limit is in Megabytes.
Requirement: cgroups (usually in a package 'libcgroup' or 'cgroup-bin')"
    exit 1
fi

if [ ! -d "/sys/fs/cgroup/memory/$USER/test" ]; then
    echo 'First create a memory cgroup usable by current user using:
    sudo cgcreate -a $USER:$USER -g memory:$USER
    cgcreate -a $USER:$USER -g memory:$USER/test'
    exit 1
fi

# Limit the memory used
echo $1M > /sys/fs/cgroup/memory/$USER/test/memory.limit_in_bytes
# Limit the memory+swap used to the same value, effectively disabling swap
echo $1M > /sys/fs/cgroup/memory/$USER/test/memory.memsw.limit_in_bytes
echo 0 > /sys/fs/cgroup/memory/$USER/test/memory.swappiness
# Acutally runs the process under the cgroup
shift
cgexec -g memory:$USER/test $@
