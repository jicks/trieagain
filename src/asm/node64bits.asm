[BITS 64]
startnode:
	push rcx
	push rax
	cmp byte [rax], 'b'                ; test if the letter is the right one
	je godown
	cmp word [rax - 1], "ba"           ; test if a swap is possible
	je godown
	cmp word [rax - 1], 'b'            ; test if a supress is possible
	je directgodown                    ; jump directy on the iteration without increasing rax
decrease:
	cmp ecx, 1                         ; check if the distance is still valid
	jle nextnode
	dec ecx
godown:
	inc rax
	directgodown:
	; sub node code
nextnode:
	pop rax
	pop rcx
	