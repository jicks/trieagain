[BITS 64]
startnode:
	push rcx
	push rax
	cmp byte [rax], 'b'                ; test if the letter is the right one
	je godown
decrease:
	cmp ecx, 1                         ; check if the distance is still valid
	jle nextnode
	dec ecx
godown:
	inc rax
	; sub node code
nextnode:
	pop rax
	pop rcx