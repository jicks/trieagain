[BITS 32]
startnode:
	push ecx
	push eax
	cmp byte [eax], 'b'                ; test if the letter is the right one
	je godown
	cmp word [eax - 1], "ba"           ; test if a swap is possible
	je godown
	cmp word [eax - 1], 'b'            ; test if a supress is possible
	je directgodown                    ; jump directy on the iteration without increasing rax
decrease:
	cmp ecx, 1                         ; check if the distance is still valid
	jle nextnode
	dec ecx
godown:
	inc eax
	directgodown:
	; sub node code
nextnode:
	pop eax
	pop ecx
	nop
	