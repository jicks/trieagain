[BITS 64]
mov rax, [rsp + 0x04 * 4]
mov rcx, [rsp + 0x04 * 5]
jmp endprolog

storagecall:
push rax
push rcx
add rdx, [rsp + 0x04 * 1 + 0x04] ; offset
push rdx
push rcx
mov rax, [rsp + 0x04 * 2 + 0x04] ; this pointer on storage
push rax
mov rax, [rsp + 0x04 * 3 + 0x04] ; function pointer
call rax
add rsp, [0x04 * 3]
pop rax
pop rcx
ret

endprolog: