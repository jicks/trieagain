[BITS 32]
mov eax, [esp + 0x04 * 4]
mov ecx, [esp + 0x04 * 5]
jmp endprolog

storagecall:
push eax
push ecx
add edx, [esp + 0x04 * 1 + 0x04] ; offset
push edx
push ecx
mov eax, [esp + 0x04 * 2 + 0x04] ; this pointer on storage
push eax
mov eax, [esp + 0x04 * 3 + 0x04] ; function pointer
call eax
add esp, [0x04 * 3]
pop eax
pop ecx
ret

endprolog: