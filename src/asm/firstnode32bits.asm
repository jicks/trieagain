[BITS 32]
startnode:
	push ecx
	push eax
	cmp byte [eax], 'b'                ; test if the letter is the right one
	je godown
decrease:
	cmp ecx, 1                         ; check if the distance is still valid
	jle nextnode
	dec ecx
godown:
	inc eax
	; sub node code
nextnode:
	pop eax
	pop ecx