#include <string>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "Loader.hh"
#include "Trie.hh"
#include "Storage.hh"

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        std::cerr << "[ERROR] Invalid command line" << std::endl;
        return 1;
    }
    std::string source = argv[1];
    std::string file = argv[1];
    Loader loader;
    if (!loader.load(file))
        return 1;

    Storage storage;
    Storage* storageptr = &storage;
    Trie trie(reinterpret_cast<unsigned char*>(loader.getRawdata()));
    while (true)
    {
        std::string word;
        unsigned int dist;

        std::cin >> word; // "approx", we just ignore it
        std::cin >> dist; // the distance
        std::cin >> word; // the word to search

        if (!std::cin.good())
            break;
        storage.prepare(loader.getRawdata(), word.c_str(), dist);
        if (dist > 0)
            trie.search(storage, word.c_str(), dist);
        else
        {
            if (!loader.execute(storageptr, 0, word.c_str()))
            {
                std::cerr << "[ERROR] Unexpected error" << std::endl;
            }
        }
        storageptr->display();
    }

    return 0;
}
