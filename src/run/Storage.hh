#ifndef STORAGE_HH_
#define STORAGE_HH_

#include <vector>

/// \class Storage
/// \brief The storage class is used to store and
///        display the valid word during a request.
class Storage
{
    public:
        Storage()
            :  _maxdistance(0), _wordsize(0), _offset(0), _word(nullptr)
        {
        }

        /// \brief Add a word to the current storage.
        /// \param[in] word the word that will be stored.
        /// \param[in] remaining the remaining distance
        ///            associated to the word.
        /// \note      This method is only used when the
        ///            native jitted trie is directly called.
        void add(char* word, int remaining);
        /// \brief Add a word to the current storage.
        /// \param[in] word the word that will be stored.
        /// \param[in] distance the DL distance between the
        ///            current word and the reference word.
        void addWord(char* word, unsigned int distance);
        /// \brief Prepare the storage for a request
        // \param[in] offset A pointer on the first byte of the jitted trie.
        // \param[in] word The reference word.
        // \param[in] distance The maximal distance that can be used.
        void prepare(void* offset, const char* word, unsigned int distance);
        /// \brief Display the result of a request formated
        ///        for a JSON like parser.
        void display();
    private:
        /// \struct StorageEntry
        /// \brief Represents a valid entry and store
        /// all the needed meta data.
        struct StorageEntry
        {
            const char* word;
            unsigned int freq;
            unsigned int dist;
        };
        int _maxdistance;
        int _wordsize;
        std::vector<StorageEntry> _entries;
        void* _offset;
        const char* _word;
};

#endif /* !STORAGE_HH_ */
