#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include "Loader.hh"
#include "Storage.hh"

static void why()
{
    std::cerr << errno << " ";
    switch (errno)
    {
        case EPERM:
        case EACCES:
            std::cerr << "Permission denied." << std::endl;
            break;
        case EBADF:
        case EBADFD:
        case ESTALE:
            std::cerr << "Invalid file descriptor." << std::endl;
            break;
        case EIO:
            std::cerr << "IO error." << std::endl;
            break;
        case EROFS:
            std::cerr << "Read only file system." << std::endl;
            break;
        case ENOTSUP:
        case ENOSYS:
            std::cerr << "Operation not supported." << std::endl;
            break;
        case ECANCELED:
            std::cerr << "Canceled." << std::endl;
            break;
        case EDQUOT:
        case EFBIG:
        case ENOMEM:
            std::cerr << "Not enought memory." << std::endl;
            break;
        case ENFILE:
        case EMFILE:
            std::cerr << "Too many file descriptors." << std::endl;
            break;
        case EISDIR:
            std::cerr << "Is a directory." << std::endl;
            break;
        case EFAULT:
            std::cerr << "Invalid address." << std::endl;
            break;
        default:
            std::cerr << "Unknow." << std::endl;
            break;
    }
}

Loader::Loader()
    : _rawdata(nullptr), _size(0), _execdic(nullptr), _fd(-1)
{
}

Loader::~Loader()
{
    munmap(_rawdata, _size);
    close(_fd);
}

bool Loader::load(const std::string& file)
{
    _fd = open(file.c_str(), O_RDONLY);
    if (_fd < 0)
    {
        std::cerr
            << "[ERROR] Impossible to load file : "
            << file
            << std::endl;
        return false;
    }
    struct stat filestat;
    fstat(_fd, &filestat);
    int pagesize = (filestat.st_size / 4096 + 1);
    _size = pagesize * 4096;
    _rawdata = mmap(nullptr, _size, PROT_EXEC | PROT_READ, MAP_PRIVATE, _fd, 0);
    if (_rawdata == static_cast<void*>(0) || _rawdata == MAP_FAILED)
    {
        std::cerr
            << "[ERROR] Error when mapping file as executable : "
            << file
            << std::endl;
        std::cerr << "        Reason: ";
        why();
        return false;
    }
    _execdic = reinterpret_cast<entry_point>(_rawdata);
    return true;
}

static void Confined(Loader::entry_point call, Storage* storage, unsigned int dist, const char* word)
{
    call(storage, &Storage::add, dist + 1, word);
}

bool Loader::execute(Storage* storage, unsigned int dist, const char* word)
{
    Storage* copy = storage;
    Confined(_execdic, copy,  dist, word);
    if (copy)
        return true;
    else
        return false;
}
