#include <algorithm>
#include <cstring>
#include <iostream>
#include "Storage.hh"

void Storage::add(char* word, int remaining)
{
    remaining--;
    char* ptrword = word + reinterpret_cast<size_t>(_offset);
    char* endword = ptrword;
    while (*endword++ != 0);
    int wsize = endword - ptrword - 1;
    //test the difference between the size of the word
    //and the size of the target
    if (wsize + _maxdistance < _wordsize ||
            wsize - _maxdistance > _wordsize)
        return;

    if (strcmp(ptrword, _word) == 0)
    {
        unsigned int freq = *reinterpret_cast<unsigned int*>(endword);
        _entries.push_back({ptrword, freq, 0});
    }
}

void Storage::addWord(char* word, unsigned int distance)
{
    char* endword = word;
    while (*endword++ != 0);
    unsigned int freq = *reinterpret_cast<unsigned int*>(endword);
    _entries.push_back({word, freq, distance});
}

void Storage::prepare(void* offset, const char* word, unsigned int distance)
{
    _offset = offset;
    _maxdistance = distance;
    _word = word;
    _wordsize = strlen(word);
    _entries.clear();
}

void Storage::display()
{
    std::sort(_entries.begin(), _entries.end(),
            [](const StorageEntry& a, const StorageEntry& b) -> bool {
                if (a.dist != b.dist) return a.dist < b.dist;
                if (a.freq != b.freq) return a.freq > b.freq;
                return strcmp(a.word, b.word) <= 0;
            });
    size_t size = _entries.size();
    if (size > 0)
    {
        size_t size_m1 = size - 1;
        std::cout << "[";
        for (size_t i = 0; i < size_m1; i++)
        {
            auto entry = _entries[i];
            std::cout << "{\"word\":\"" << entry.word
                << "\",\"freq\":" << entry.freq
                << ",\"distance\":" << entry.dist << "},";
        }
        {
            auto entry = _entries[size_m1];
            std::cout << "{\"word\":\"" << entry.word
                << "\",\"freq\":" << entry.freq
                << ",\"distance\":" << entry.dist << "}]"
                << std::endl;
        }
    }
    else
    {
        std::cout << "[]" << std::endl;
    }
}
