#ifndef TRIE_HH_

# define TRIE_HH_

# include "Storage.hh"
# include "AsmMap.hh"

class Trie
{
    public:
        Trie(unsigned char* rawdata);
        ~Trie();
        void load(unsigned char* rawdata);
        void search(Storage& storage, const char* word, unsigned int distance);
    private:
        bool _search(char oldc, unsigned int* previousRow, unsigned int* transRow);
        unsigned char* _rawdata;
        AsmMap _node;
        Storage* _storage;
        const char* _word;
        size_t _length;
        size_t _realLength;
        unsigned int _maxDistance;
        unsigned int* _cachedMemory;
};

#endif /* !TRIE_HH_ */
