#include "Trie.hh"
#include <cstring>
#include <iostream>

static unsigned int min(unsigned int a, unsigned int b, unsigned int c, unsigned int d)
{
    unsigned int e = b ^ ((a ^ b) & -(a < b));
    unsigned int f = d ^ ((c ^ d) & -(c < d));
    return f ^ ((e ^ f) & -(e < f));
}

Trie::Trie(unsigned char* rawdata)
    : _rawdata(rawdata), _node(rawdata), _storage(nullptr), _word(nullptr),
      _length(0), _realLength(0), _maxDistance(0), _cachedMemory(nullptr)
{
}

Trie::~Trie()
{
    delete[] _cachedMemory;
}

void Trie::load(unsigned char* rawdata)
{
    _node = AsmMap(rawdata);
}

void Trie::search(Storage& storage, const char* word, unsigned int distance)
{
    _node = AsmMap(_rawdata);
    _storage = &storage;
    _word = word;
    _length = strlen(word);
    _maxDistance = distance;
    if (_length > _realLength)
    {
        delete _cachedMemory;
        _cachedMemory = new unsigned int[(_length + 1) * (_node.getHeight() + 1)];
        _realLength = _length;
    }
    unsigned int* currentRow = _cachedMemory;

    for (size_t i = 0; i <= _length; i++)
        currentRow[i] = i;

    while (!_node.ended())
    {
        if (!_search(' ', currentRow, nullptr))
            _node.nextNode();
    }
}

bool Trie::_search(char oldc, unsigned int* previousRow, unsigned int* transRow)
{
    unsigned int* currentRow = _cachedMemory + ((_node.level() + 1) * (_length + 1));
    char c = _node.character();

    currentRow[0] = previousRow[0] + 1;

    // Build one row for the letter, with a column for each letter in the target
    // word, plus one for the empty string at column 0
    unsigned int minimum = currentRow[0];
    {
        unsigned int insertCost = currentRow[0] + 1;
        unsigned int deleteCost = previousRow[1] + 1;
        unsigned int replaceCost = previousRow[0] + (_word[0] != c);
        unsigned int transCost = 100;
        currentRow[1] = min(insertCost, deleteCost, replaceCost, transCost);
        if (currentRow[1] < minimum)
            minimum = currentRow[1];
    }
    for (size_t column = 2; column <= _length; column++)
    {
        unsigned int insertCost = currentRow[column - 1] + 1;
        unsigned int deleteCost = previousRow[column] + 1;
        unsigned int replaceCost = previousRow[column - 1] + (_word[column - 1] != c);
        unsigned int transCost = 100;

        if (transRow && _word[column - 1] == oldc &&
                        _word[column - 2] == c)
            transCost = transRow[column - 2] + 1;
        currentRow[column] = min(insertCost, deleteCost, replaceCost, transCost);
        if (currentRow[column] < minimum)
            minimum = currentRow[column];
    }

    // if the last entry in the row indicates the optimal cost is less than the
    // maximum cost, and there is a word in this trie node, then add it.
    if (currentRow[_length] <= _maxDistance && _node.isFinal())
        _storage->addWord(_node.word(), currentRow[_length]);

    // if any entries in the row are less than the maximum cost, then
    // recursively search each branch of the trie
    if (minimum <= _maxDistance && _node.subNode())
    {
        int level = _node.level();
        while (_node.level() == level)
        {
            if (!_search(c, currentRow, previousRow))
                _node.nextNode();
        }

        return true;
    }

    return false;
}
