#ifndef LOADER_HH_
#define LOADER_HH_

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>

#include "Storage.hh"

/// \class Loader
/// \brief A class used to load a dictionary.
class Loader
{
    public:
        Loader();
        ~Loader();
        /// \brief Load a dictionary.
        /// \param[in] file The path of the file that store the dictionary.
        /// \return true if succeed false otherwise.
        bool load(const std::string& file);
        /// \brief Return the pointer to the loaded file
        void* getRawdata() { return _rawdata; }
        /// \brief Directly jump on the emitted asm.
        /// \param[in][out] storage the storage that will be filled during the request.
        /// \param[in]      dist the distance associated to the request.
        /// \param[in]      word the word associated to the request.
        bool execute(Storage* storage, unsigned int dist, const char* word);
        typedef void (Storage::*add_function)(char* word, int remain);
        typedef int (*entry_point)(Storage* storage, add_function fct, unsigned int dist, const char* word);
    private:
        

        void* _rawdata;
        size_t _size;
        entry_point _execdic;
        int _fd;
};

#endif /* !LOADER_HH_ */
