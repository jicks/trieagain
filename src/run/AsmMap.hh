#ifndef ASMMAP_HH
#define ASMMAP_HH

#include <iostream>
#define AS_INT(ptr) (*reinterpret_cast<int*>(ptr))
#define JMP(ptr, value) (ptr + (value + 4))
/// \class AsmMap
/// \brief Used to access to the dictionary as a usual
///        trie abstracting the underlying asm.
class AsmMap
{
public:
    AsmMap(unsigned char* data) : _data(data), _level(0), _maxheight(0)
    {
        // Skip the local initialization
        _data += 16 + 1;
        int offset = AS_INT(_data);
        _maxheight = AS_INT(_data + 4);
        _data = JMP(_data, offset);
        _data++;
        offset = AS_INT(_data);
        _callcpp = (_data + 4);
        _data = JMP(_data, offset);
        _nextchunk = _data;
        _skipNode();
    }
    AsmMap(const AsmMap& clone)
        :
    _data(clone._data),
    _nextchunk(clone._nextchunk),
    _subchunk(clone._subchunk),
    _word(clone._word),
    _c(clone._c),
    _level(clone._level),
    _callcpp(clone._callcpp),
    _maxheight(clone._maxheight)
    {
    }
    /// \brief Jump to the next node. If the curent node was
    ///        the last node of the level false is returned and
    ///        level is updated.
    bool nextNode()
    {
        // Skip the two pop
        _nextchunk += 2;
        bool flag = true;
loop:
            if (*_nextchunk == 0x58)
            {
                flag = false;
                _nextchunk += 2;
                _level--;
                goto loop;
            }
            else if (*_nextchunk == 0xEB)
            {
                _nextchunk += 1;
                _nextchunk += 1 + (int)*_nextchunk + 10;
                goto loop;
            }
            else if (*_nextchunk == 0xE9)
            {
                _nextchunk += 1;
                 int offset = AS_INT(_nextchunk);
                _nextchunk = JMP(_nextchunk, offset);
                _nextchunk += 10;
                goto loop;
            }
            if (*_nextchunk == 0xC3)
            {
                return false;
            }
        _skipNode();
        return flag;
    }
    /// \brief Get the character associated to the current node.
    char  character() { return _c; }
    /// \brief Test if this node contain a word.
    bool  isFinal() { return _word != nullptr; }
    /// \brief Get the maximum height of the trie
    unsigned int getHeight() { return _maxheight; }
    /// \brief Get the word associated to this node.
    char* word() { return _word; }
    /// \brief Jump on the first child of this node.
    /// \return true if the chil exist. false otherwise.
    bool subNode()
    {
        if (_subchunk == nullptr)
            return false;
        _level++;
        _nextchunk = _subchunk;
        _skipNode();
        return true;
    }
    /// \brief Get the current level.
    int level()
    {
        return _level;
    }
    /// \brief The flag ended is set to true when there is no more
    /// node to read.
    bool ended()
    {
        return  (*_nextchunk == 0xC3);
    }
private:
    /// \brief Parse the asm in order to extract the word stored in rax.
    void _reverseParseFinalNode(unsigned char* ptr)
    {
        ptr -= 5 + 5 + 4 + 1 + 1;
        while (*ptr != 0xEB && *ptr != 0xE9)
        {
            ptr--;
        }
        if (*ptr == 0xEB)
            ptr += 2;
        else
            ptr += 5;
        _word = reinterpret_cast<char*>(ptr);
    }
    /// \brief Load the noderepresented by the current pointer
    ///        and update all the flags.
    bool _skipNode()
    {
        _word = nullptr;
        _data = _nextchunk;
        // push | 1
        // push | 1
        // cmp  | 1 1
        _data += 4; // skip the push push
        _c = *_data;
        // jle XX
        _data += 2;
        _subchunk = _data + (int)*_data + 1;
        if (*_subchunk == 0x40)
            _subchunk++;
        else
            _subchunk = nullptr;

        /* Uncomment me if you want to use the full heuristic
        if (_level > 0)
        {
            // 3 compare and 3 jump
            _data += 3 * 2 + 3 + 4 + 6;
        }
        */
        // cmp
        // jle NEXT
        _data += 3 + 2 + 1;
        _nextchunk = JMP(_data, AS_INT(_data));
        if (_nextchunk[-5] == 0xE8)
        {
            if (_nextchunk + AS_INT(_nextchunk - 4)  == _callcpp)
            {
                _reverseParseFinalNode(_nextchunk);
            }
        }
        // End of node
        return false;
    }

    unsigned char* _data;
    unsigned char* _nextchunk;
    unsigned char* _subchunk;
    unsigned int _maxheight;
    char* _word;
    char  _c;
    int _level;
    unsigned char* _callcpp;

};
#endif
