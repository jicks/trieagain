#ifndef TOOLS_BYTEVECTOR_HH_
#define TOOLS_BYTEVECTOR_HH_
#include <unistd.h>
#include <stdlib.h>
/// \class Bytevector
/// \brief A memory efficient vector.
class Bytevector
{
public:
  Bytevector() :
    _data(nullptr),
    _size(0),
    _capacity(0)
  {
    _capacity = 4096;
    _data = (char*)malloc(sizeof(char) * (_capacity));
  }
  ~Bytevector()
  {
    if (_data != 0)
    {
      free(_data);
      _data = 0;
    }
  }
  /// \brief Add a new value add the end of the vector.
  /// \param[in] value The data that should be added.
  void push_back(char value)
  {
    if (_size + 1 > _capacity)
    {
      _capacity += 4096 * 4;
      _data = (char*)realloc(_data, sizeof(char) * _capacity);
    }
    _data[_size] = value;
    _size++;
  }
  /// \brief Remove all the element stored into the vector.
  void clear()
  {
    _size = 0;
  }
  /// \brief Get a raw pointer on the first element of the vector.
  char* raw()
  {
    return _data;
  }
  /// \brief The element at the specified poistion.
  /// \param[in] at The position of the element that should be returned.
  char& operator[](int at)
  {
    return _data[at];
  }
  /// \brief Get the size of the vector.
  int size()
  {
    return _size;
  }
  /// \brief Get the allocated space by the vector.
  int capacity()
  {
    return _capacity;
  }
private:
  char* _data;
  int _size;
  int _capacity;
};

#endif
