#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include "Trie.hh"
#include "Emit.hh"

static int convert(char** ptr)
{
  char* tptr = *ptr;
  int value = 0;
  while (*tptr >= '0' && *tptr <='9')
  {
    value *= 10;
    value += *tptr - '0';
    ++tptr;
  }
  while (*tptr >= '\t' && *tptr <= ' ')
    ++tptr;
  *ptr = tptr;
  return value;
}

int main(int argc, char** argv)
{
    if (argc != 3)
    {
        std::cerr << "[ERROR] Invalid command line" << std::endl;
        return 1;
    }

    Trie trie;
    int fd = open(argv[1], O_RDONLY);
    struct stat filestat;
    fstat(fd, &filestat);
    char* file = (char*)mmap(nullptr, filestat.st_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
    char* endfile = file + filestat.st_size;
    while (file < endfile)
    {
        char* word = file;
        while (!isspace(*file))
            file++;
        *(file++) = 0;
        size_t frequency = convert(&file);
        trie.addWord(word, frequency);
    }

    Emitter emit;
    emit.beginsave(argv[2]);
    emit.prologue(trie.getHeight());
    emit.flush();
    for (auto& subnode : trie.getRoot()->edges())
    {
        emit.createfirstnode(&subnode.second, subnode.first);
	    emit.flush();
    }
    emit.ret();
    emit.flush();
    emit.endsave();
    munmap(endfile - filestat.st_size, filestat.st_size);
    close(fd);
    return 0;
}
