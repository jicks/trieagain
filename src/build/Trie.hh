#ifndef TRIE_HH_

# define TRIE_HH_

# include <ostream>
# include <vector>

/// \class Answer
/// \brief Represents the result of a request.
class Answer {
    public:
        /// \brief add a word to the set of word matching
        /// the request.
        /// \param[in] word the word that should be added.
        /// \param[in] frequency the frequency associated to the word.
        /// \param[in] distance the distance associated to the word.
        void addWord(char* word, size_t frequency, size_t distance);
        /// \brief Dump the result into a stream.
        /// \param[in] out The stream that will be
        ///            used to dump the result.
        void write(std::ostream& out);
    private:
        /// \brief Store one word that match the specified request
        /// and all the associated meta data.
        struct AnswerEntry {
            char* word;
            size_t freq;
            size_t dist;
        };
        std::vector<AnswerEntry> entries_;

};

/// \class Node
/// \brief Represents a node of the Trie.
class Node {
    public:
        Node(size_t level) : edges_(), word_(), frequency_(0), level_(level) { }
        ~Node() { }
        /// \brief Add an aedge to the current node and create
        /// the child node.
        /// \param[in] c the character associated to the transition.
        Node* addEdge(char c);
        /// \brief Get the frequency stored in this node.
        size_t getFrequency() const;
        /// \brief Add a word in the trie.
        /// This method will performed all needed operations(creations)
        /// to add a new word in the trie.
        /// \param[in] word The word that will be added in the trie.
        /// \param[in] frequency The frequency associated to the word.
        void addWord(char* word, size_t frequency);
        /// \brief Get the word associated to this node.
        char* getWord();
        /// \brief Test if this node is a final node
        /// (i.e. if the node contain a word)
        bool isFinal() const;
        /// \brief Return the level of the node in the trie
        size_t getLevel() const { return level_; }
        /// \brief Try to find a node in the trie.
        /// \param[in/out] answer the result of tthe request.
        /// \param[in] word the reference word associated to the request.
        /// \param[in] dist the current distance used as a variable during recursion.
        /// \param[in] maxdist the distance associated to the request.
        void search(Answer& answer, char* word, size_t dist, size_t maxDist);
        /// \brief Get the list of all the children of this node.
        std::vector<std::pair<char, Node> >& edges() { return edges_; }
    private:
        std::vector<std::pair<char, Node> > edges_;
        char* word_;
        size_t frequency_;
        size_t level_;
};

/// \class Trie
/// \brief A class used to represent a Trie during the build.
class Trie {
    public:
        Trie() : root_(new Node(0)), maxLevel_(0) {}
        ~Trie() { delete root_; }
        /// \brief Add a new word to the trie.
        /// \param[in] word A pointer on the word
        ///             that will be added to the trie.
        /// \param[in] frequency The frequency associated to the word.
        void addWord(char* word, size_t frequency);
        /// \brief Retrieve a pointer on the root of the trie.
        Node* getRoot();
        /// \brief Return the height of the trie.
        size_t getHeight() const { return maxLevel_; }
        /// \brief Try to find a word in the trie.
        /// \param[in] word the word that will be searched in the trie.
        /// \param[in] distance the distance associated with the request.
        Answer searchWord(char* word, size_t distance);
    private:
        Node* root_;
        size_t maxLevel_;
};

#endif /* !TRIE_HH_ */
