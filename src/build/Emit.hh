#ifndef EMIT_HH
#define EMIT_HH

#include "Trie.hh"
#include <bytevector.hh>
#include <map>
#include <vector>
#include <string>
#include <fstream>

/// \namespace Processor
///`\brief Processor specific enumerations.
namespace Processor
{
#ifdef PROC_X86
    enum Register : unsigned char
    {
        eax = 0x00,
        ebx = 0x03,
        ecx = 0x01,
        edx = 0x02,
        esp = 0x04,
        ebp = 0x05
    };
#endif
#ifdef PROC_X64
    enum Register : unsigned char
    {
        rax = 0x00,
        rbx = 0x03,
        rcx = 0x01,
        rdx = 0x02,
        rsp = 0x04,
        rbp = 0x05,
        r8 = 0x10,
        r9 = 0x11,
        r10 = 0x12,
        r11 = 0x13,
        r12 = 0x14,
    };
#endif
}
/// \class Emitter
/// \brief Used for native code generation.
class Emitter
{
    public:
        Emitter() : _offset(0), _callstorage(0), _block(){}
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): push reg
        void push(Processor::Register reg);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): pop reg
        void pop(Processor::Register reg);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): call reg
        void call(Processor::Register reg);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): cmp reg, 'chr'
        void cmp(Processor::Register reg, char chr);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): cmp byte [reg], 'chr'
        void cmpat(Processor::Register reg, char chr);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): cmp byte [reg + offset], 'chr'
        void cmpat(Processor::Register reg, char offset, char chr);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): cmp word [reg + offset], 'chr' 'chr2'
        void cmpat(Processor::Register reg, char offset, char chr1, char chr2);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): ret
        void ret();
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): mov reg, [sp + index]
        void getlocal(Processor::Register reg, unsigned char index);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): add reg, [sp + index]
        void addlocal(Processor::Register reg, unsigned char index);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): add reg, value
        void add(Processor::Register reg, unsigned char value);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): mov reg, value
        void mov(Processor::Register reg, void* value);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): inc reg
        void inc(Processor::Register reg);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): dec reg
        void dec(Processor::Register reg);

        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): call ?address?;
        ///        where ?address? should be specified later by using reloc.
        int rcall();
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): call address
        void call(int address);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): jmp ?address?;
        ///        where ?address? should be specified later by using reloc.
        int rjmp();
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): jmp address
        void jmp(int address);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): jmp ?address?;
        ///        ;where ?address? should be specified later by using reloc.
        /// \note The address is emitted as a relative address using one byte.
        int smallrjmp();
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): jmp address
        /// \note The address is emitted as a relative address using one byte.
        void smalljmp(int address);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): je address
        int rje();
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): je ?address?
        ///        ;where ?address? should be specified later by using reloc.
        void je(int address);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): je ?address?
        ///        ;where ?address? should be specified later by using reloc.
        /// \note The address is emitted as a relative address using one byte.
        int smallrje();
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): je address
        /// \note The address is emitted as a relative address using one byte.
        void smallje(int address);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): jne ?address?
        int rjne();
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): jne address
        void jne(int address);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): jle ?address?
        int rjle();
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): jle address
        void jle(int address);
        /// \brief Create a label that can be used by the reloc function.
        int label();
        ///`\brief Perform a relocation.
        /// \note  If a small address has been used you __MUST__
        ///        use smallreloc instead of reloc.
        void reloc(int source, int address);
        ///`\brief Perform a relocation on a small address.
        /// \note  If a long address has been used you __MUST__
        ///        use reloc instead of smallreloc.
        void smallreloc(int source, int address);
        /// \brief Emit an asm instruction in the output stream.
        ///        * Instruction (Intel syntax): nop reg
        void nop();
        /// \brief Emit a GCC compliant prologue that will be used
        ///        by the emitted asm to interop with gcc generated code.
        void prologue(int maxdepth);
        /// \brief Emit a top level node.
        /// \param[in] node The node tha should be converted into asm.
        /// \param[in] c    The char that is stored on the edge of the node.
        void createfirstnode(Node* node, char c);
        /// \brief Emit a bottom level node.
        /// \param[in] node The node tha should be converted into asm.
        /// \param[in] c    The char that is stored on the edge of the node.
        void createfinalnode(Node* node, char c);
         /// \brief Emit a middle level node.
        /// \param[in] node The node tha should be converted into asm.
        /// \param[in] c    The char that is stored on the edge of the node.
        /// \param[in] oldc    The char that is stored on the edge
        ///                    of the parent node.
        void createnode(Node* node, char c, char oldc);

        /// \brief Open the output stream.
        /// \param[in] file The file where the emitted asm
        ///            will be stored.
        void beginsave(const std::string& file);
        /// \brief Close the output stream.
        void endsave();
        /// \brief Flush the output stream.
        void flush();
    private:
        int _offset;
        Bytevector _block;
        std::ofstream _output;
        int _callstorage;
};

#endif