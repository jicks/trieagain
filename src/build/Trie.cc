#include <algorithm>
#include <cstring>
#include <iostream>

#include "Trie.hh"

void Answer::addWord(char* word, size_t frequency, size_t distance) {
    entries_.push_back({word, frequency, distance});
}

void Answer::write(std::ostream& out) {
    std::sort(entries_.begin(), entries_.end(),
            [](const AnswerEntry& a, const AnswerEntry& b) -> bool {
                if (a.dist != b.dist) return a.dist < b.dist;
                if (a.freq != b.freq) return a.freq > b.freq;
                return strcmp(a.word, b.word) <= 0;
            });
    out << "[";
    for (size_t i = 0; i < entries_.size() - 1; i++)
        out << "{\"word\":\"" << entries_[i].word << "\","
            << "\"freq\":" << entries_[i].freq << ","
            << "\"distance\":" << entries_[i].dist << "},";
    out << "{\"word\":\"" << entries_.back().word << "\","
        << "\"freq\":" << entries_.back().freq << ","
        << "\"distance\":" << entries_.back().dist << "}]" << std::endl;
}

Node* Node::addEdge(char c) {
    for (size_t i = 0; i < edges_.size(); i++)
        if (edges_[i].first == c)
            return &edges_[i].second;
    edges_.push_back(std::make_pair(c, Node(level_ + 1)));
    return &edges_.back().second;
}

void Node::addWord(char* word, size_t frequency) {
    word_ = word;
    frequency_ += frequency;
}

size_t Node::getFrequency() const {
    return frequency_;
}

char* Node::getWord() {
    return word_;
}

bool Node::isFinal() const {
    return frequency_ != 0;
}

void Node::search(Answer& answer, char* word, size_t dist, size_t maxDist) {

    if (dist > maxDist)
        return;

    if (isFinal() && (strlen(word) <= maxDist - dist))
        answer.addWord(word_, frequency_, dist);

    // deletion
    if (word[0])
        search(answer, word+1, dist+1, maxDist);
    for (auto& edge: edges_) {
        // substitution
        if (word[0])
            edge.second.search(answer, word+1, dist+(edge.first != word[0]), maxDist);
        // insertion
        edge.second.search(answer, word, dist+1, maxDist);
        // transposition
        if (word[0] && word[1] && edge.first == word[1]) {
            char c = word[1]; word[1] = word[0];
            edge.second.search(answer, word+1, dist+1, maxDist);
            word[1] = c;
        }
    }
}

void Trie::addWord(char* word, size_t frequency) {
    Node* node = root_;
    for (size_t i = 0; word[i]; i++)
    {
        node = node->addEdge(word[i]);
        if (node->getLevel() > maxLevel_)
            maxLevel_ = node->getLevel();
    }
    node->addWord(word, frequency);
}

Node* Trie::getRoot() {
    return root_;
}

Answer Trie::searchWord(char* word, size_t distance) {
    Answer answer;
    root_->search(answer, word, 0, distance);
    return answer;
}
