#include "Emit.hh"
#include "Trie.hh"
#include <fstream>
#include <assert.h>

#ifdef PROC_X86
#define PROC_FAM_X86
#define SIZE_PTR 0x4
#define R1 (Processor::eax)
#define R2 (Processor::ebx)
#define R3 (Processor::ecx)
#define R4 (Processor::edx)

#define STACKPTR (Processor::esp)

void Emitter::push(Processor::Register reg)
{
    _block.push_back(0x50 + reg);
}
void Emitter::pop(Processor::Register reg)
{
    _block.push_back(0x58 + reg);
}

void  Emitter::call(Processor::Register reg)
{
    _block.push_back(0xFF);
    _block.push_back(0xD0 + reg);
}

void Emitter::cmpat(Processor::Register reg, char chr)
{
    _block.push_back(0x80);
    _block.push_back(0x38 + reg);
    _block.push_back(reinterpret_cast<char>(chr));
}

void Emitter::cmpat(Processor::Register reg, char offset, char chr)
{
    if (offset == 0)
    {
        cmpat(reg, chr);
        return;
    }
    _block.push_back(0x80);
    _block.push_back(0x78 + reg);
    _block.push_back(offset);
    _block.push_back(reinterpret_cast<char>(chr));
}

void Emitter::cmpat(Processor::Register reg, char offset, char chr1, char chr2)
{
    _block.push_back(0x66);
    _block.push_back(0x81);
    _block.push_back(0x78 + reg);
    _block.push_back(offset);
    _block.push_back(reinterpret_cast<char>(chr1));
    _block.push_back(reinterpret_cast<char>(chr2));

}


void Emitter::cmp(Processor::Register reg, char chr)
{
    _block.push_back(0x83);
    _block.push_back(0xF8 + reg);
    _block.push_back(reinterpret_cast<char>(chr));
}
void Emitter::getlocal(Processor::Register reg, unsigned char index)
{
    _block.push_back(0x8b);
    if      (reg == Processor::eax) _block.push_back(0x44);
    else if (reg == Processor::ebx) _block.push_back(0x5C);
    else if (reg == Processor::ecx) _block.push_back(0x4C);
    else if (reg == Processor::edx) _block.push_back(0x54);
    else assert("Instruction not supported");
    _block.push_back(0x24);
    _block.push_back(index * 0x04);
}

void Emitter::addlocal(Processor::Register reg, unsigned char index)
{
    _block.push_back(0x03);
    if      (reg == Processor::eax) _block.push_back(0x44);
    else if (reg == Processor::ebx) _block.push_back(0x5C);
    else if (reg == Processor::ecx) _block.push_back(0x4C);
    else if (reg == Processor::edx) _block.push_back(0x54);
    else assert("Instruction not supported");
    _block.push_back(0x24);
    _block.push_back(index * 0x04);
}

void Emitter::inc(Processor::Register reg)
{
    _block.push_back(0x40 + reg);
}

void Emitter::dec(Processor::Register reg)
{
    _block.push_back(0x48 + reg);
}
void Emitter::add(Processor::Register reg, unsigned char value)
{
    _block.push_back(0x83);
    _block.push_back(0xC0 + reg);
    _block.push_back(value);

}

void Emitter::mov(Processor::Register reg, void* value)
{
    char* ptr = reinterpret_cast<char*>(&value);

    _block.push_back(0xB8 + reg);

    _block.push_back(ptr[0]);
    _block.push_back(ptr[1]);
    _block.push_back(ptr[2]);
    _block.push_back(ptr[3]);
}

#elif PROC_X64
#define PROC_FAM_X86
#define SIZE_PTR 0x8
#define R1 (Processor::rax)
#define R2 (Processor::rbx)
#define R3 (Processor::rcx)
#define R4 (Processor::rdx)

#define STACKPTR (Processor::rsp)

void Emitter::push(Processor::Register reg)
{
    if (reg >= Processor::r8)
    {
        _block.push_back(0x41);
        _block.push_back(0x40 + reg);
    }
    else
    {
        _block.push_back(0x50 + reg);
    }
}
void Emitter::pop(Processor::Register reg)
{
    if (reg >= Processor::r8)
    {
        _block.push_back(0x41);
        _block.push_back(0x48 + reg);
    }
    else
    {
        _block.push_back(0x58 + reg);
    }
}
void  Emitter::call(Processor::Register reg)
{
    if (reg >= Processor::r8)
    {
        _block.push_back(0x41);
        _block.push_back(0xFF);
        _block.push_back(0xC0 + reg);
    }
    else
    {
        _block.push_back(0xFF);
        _block.push_back(0xD0 + reg);
    }
}

void Emitter::cmpat(Processor::Register reg, char chr)
{
    if (reg >= Processor::r8)
    {
        assert("Instruction not supported");
    }
    _block.push_back(0x80);
    _block.push_back(0x38 + reg);
    _block.push_back(reinterpret_cast<char>(chr));
}

void Emitter::cmpat(Processor::Register reg, char offset, char chr)
{
    if (reg >= Processor::r8)
    {
        assert("Instruction not supported");
    }
    if (offset == 0)
    {
        cmpat(reg, chr);
        return;
    }
    _block.push_back(0x80);
    _block.push_back(0x78 + reg);
    _block.push_back(offset);
    _block.push_back(reinterpret_cast<char>(chr));
}

void Emitter::cmpat(Processor::Register reg, char offset, char chr1, char chr2)
{
    if (reg >= Processor::r8)
    {
        assert("Instruction not supported");
    }
    _block.push_back(0x66);
    _block.push_back(0x81);
    _block.push_back(0x78 + reg);
    _block.push_back(offset);
    _block.push_back(reinterpret_cast<char>(chr1));
    _block.push_back(reinterpret_cast<char>(chr2));

}

void Emitter::cmp(Processor::Register reg, char chr)
{
    if (reg >= Processor::r8)
    {
        assert("Instruction not supported");
    }
    else
    {
        _block.push_back(0x48);
        _block.push_back(0x83);
        _block.push_back(0xF8 + reg);
        _block.push_back(reinterpret_cast<char>(chr));
    }
}

void Emitter::getlocal(Processor::Register reg, unsigned char index)
{
    _block.push_back(0x48);
    _block.push_back(0x8B);
    if      (reg == Processor::rax) _block.push_back(0x44);
    else if (reg == Processor::rbx) _block.push_back(0x5C);
    else if (reg == Processor::rcx) _block.push_back(0x4C);
    else if (reg == Processor::rdx) _block.push_back(0x54);
    else assert("Instruction not supported");
    _block.push_back(0x24);
    _block.push_back(index * 0x08);
}

void Emitter::addlocal(Processor::Register reg, unsigned char index)
{
    _block.push_back(0x48);
    _block.push_back(0x03);
    if      (reg == Processor::rax) _block.push_back(0x44);
    else if (reg == Processor::rbx) _block.push_back(0x5C);
    else if (reg == Processor::rcx) _block.push_back(0x4C);
    else if (reg == Processor::rdx) _block.push_back(0x54);
    else assert("Instruction not supported");
    _block.push_back(0x24);
    _block.push_back(index * 0x08);
}

void Emitter::inc(Processor::Register reg)
{
    if (reg >= Processor::r8)
    {
        assert("Instruction not supported");
    }
    else
    {
        _block.push_back(0x48);
        _block.push_back(0xFF);
        _block.push_back(0xC0 + reg);
    }
}

void Emitter::dec(Processor::Register reg)
{
    if (reg >= Processor::r8)
    {
        assert("Instruction not supported");
    }
    else
    {
        _block.push_back(0x48);
        _block.push_back(0xFF);
        _block.push_back(0xC8 + reg);
    }
}
void Emitter::add(Processor::Register reg, unsigned char value)
{
    if (reg >= Processor::r8)
    {
        assert("Instruction not supported");
    }
    else
    {
        _block.push_back(0x48);
        _block.push_back(0x83);
        _block.push_back(0xC0 + reg);
        _block.push_back(value);
    }
}

void Emitter::mov(Processor::Register reg, void* value)
{
    if (reg >= Processor::r8)
    {
        assert("Instruction not supported");
    }

    char* ptr = reinterpret_cast<char*>(&value);

    _block.push_back(0x48);
    _block.push_back(0xB8 + reg);

    _block.push_back(ptr[0]);
    _block.push_back(ptr[1]);
    _block.push_back(ptr[2]);
    _block.push_back(ptr[3]);

    _block.push_back(ptr[4]);
    _block.push_back(ptr[5]);
    _block.push_back(ptr[6]);
    _block.push_back(ptr[7]);
}

#else
#error "Processor not supported"
#endif

#ifdef PROC_FAM_X86

void  Emitter::jle(int address)
{
    reloc(rje(), address);
}

int Emitter::rjle()
{
    _block.push_back(0x0F);
    _block.push_back(0x8E);
    int at = _block.size() + _offset;
    _block.push_back(0x0);
    _block.push_back(0x0);
    _block.push_back(0x0);
    _block.push_back(0x0);
    return at;
}

void  Emitter::je(int address)
{
    reloc(rje(), address);
}

int Emitter::rje()
{
    _block.push_back(0x0F);
    _block.push_back(0x84);
    int at = _block.size() + _offset;
    _block.push_back(0x0);
    _block.push_back(0x0);
    _block.push_back(0x0);
    _block.push_back(0x0);
    return at;
}

void  Emitter::smallje(int address)
{
    reloc(smallrje(), address);
}

int Emitter::smallrje()
{
    _block.push_back(0x74);
    int at = _block.size() + _offset;
    _block.push_back(0x0);
    return at;
}

void  Emitter::jne(int address)
{
    reloc(rjne(), address);
}

int Emitter::rjne()
{
    _block.push_back(0x0F);
    _block.push_back(0x85);
    int at = _block.size() + _offset;
    _block.push_back(0x0);
    _block.push_back(0x0);
    _block.push_back(0x0);
    _block.push_back(0x0);
    return at;
}

void  Emitter::jmp(int address)
{
    reloc(rjmp(), address);
}
int Emitter::rjmp()
{
    _block.push_back(0xE9);
    int at = _block.size() + _offset;
    _block.push_back(0x0);
    _block.push_back(0x0);
    _block.push_back(0x0);
    _block.push_back(0x0);
    return at;
}

void  Emitter::smalljmp(int address)
{
    reloc(smallrjmp(), address);
}
int Emitter::smallrjmp()
{
    _block.push_back(0xEB);
    int at = _block.size() + _offset;
    _block.push_back(0x0);
    return at;
}

void  Emitter::call(int address)
{
    reloc(rcall(), address);
}


int Emitter::rcall()
{
    _block.push_back(0xE8);
    int at = _block.size() + _offset;
    _block.push_back(0x0);
    _block.push_back(0x0);
    _block.push_back(0x0);
    _block.push_back(0x0);
    return at;
}
void Emitter::ret()
{
    _block.push_back(0xC3);
}
void Emitter::nop()
{
    _block.push_back(0x90);
}
#endif

void Emitter::reloc(int at, int address)
{
    int offset = address - (at + 4);
    char* ptr = reinterpret_cast<char*>(&offset);
    at -= _offset;
    _block[at + 0] = ptr[0];
    _block[at + 1] = ptr[1];
    _block[at + 2] = ptr[2];
    _block[at + 3] = ptr[3];
}

void Emitter::smallreloc(int at, int address)
{
    int offset = address - (at + 1);
    if (offset >= 127 || offset <= -126)
        assert("Small reloc can't be used. Range is too big.");
    char* ptr = reinterpret_cast<char*>(&offset);
    at -= _offset;
    _block[at + 0] = ptr[0];
}

int Emitter::label()
{
    return _block.size() + _offset;
}
void Emitter::prologue(int maxdepth)
{
    getlocal(R1, 5);
    getlocal(R3, 4);
    getlocal(R2, 2);
    getlocal(R4, 1);
    auto r_jump_endmetadata = rjmp();
    char* ptr = reinterpret_cast<char*>(&maxdepth);
    _block.push_back(ptr[0]);
    _block.push_back(ptr[1]);
    _block.push_back(ptr[2]);
    _block.push_back(ptr[3]);
    auto endmetadata = label();
    reloc(r_jump_endmetadata, endmetadata);
    auto r_jump_endprolog = rjmp();
    auto storagecall = label();
    _callstorage = storagecall;
    //Used for restore
    //push(R3);  always performed after
    //push(R1);
    push(R4);

    //Function call
    push(R3);
    push(R1);
    push(R4);
    
    call(R2);

    // Clean stack
    add(STACKPTR, SIZE_PTR * 3);

    // restore
    pop(R4);
    //pop(R1);
    //pop(R3); always performed after
    ret();
    auto endprolog = label();
    reloc(r_jump_endprolog, endprolog);
}

void Emitter::createfirstnode(Node* node, char c)
{
    push(R3);
    push(R1);
    cmpat(R1, c);
    auto reloc_godown = smallrje();
    // Decrease the distance
    cmp(R3, 1);
    auto reloc_nextnode = rje();
    dec(R3);
    auto godown = label();
    smallreloc(reloc_godown, godown);
    {
        inc(R1);
        {
            for(auto& subnode : node->edges())
            {
                createnode(&subnode.second, subnode.first, c);
            }
        }
    }
    // Test if this node is a final node
    if (node->isFinal())
    {
        createfinalnode(node, c);
    }
    auto nextnode = label();
    reloc(reloc_nextnode, nextnode);
    pop(R1);
    pop(R3);
}

void Emitter::createnode(Node* node, char c, char oldc)
{
    push(R3);
    push(R1);
    //replace
    cmpat(R1, c);
    auto reloc_godown1 = smallrje();
    // Uncomment this block if you want a better heuristic
    // Warning it takes a lot more memory.
    /*
    //swap
    cmpat(R1, -1, c, oldc);
    auto reloc_godown2 = smallrje();
    //suppress
    cmpat(R1, oldc);
    auto reloc_godown3 = smallrje();
    //insert
    //FIXME I have a doubt on the validity of the insertion
    // it seems to accept some words that should be rejected
    cmpat(R1, -1, c);
    auto reloc_godown4 = smallrje();*/
    cmp(R3, 1);
    auto reloc_nextnode = rje();
    dec(R3);
    auto godown = label();
    smallreloc(reloc_godown1, godown);
    //smallreloc(reloc_godown2, godown);
    //smallreloc(reloc_godown3, godown);
    //smallreloc(reloc_godown4, godown);
    if (node->edges().size() > 0)
    {
        inc(R1);
        auto directgodown = label();
        {
            for(auto& subnode : node->edges())
            {
                createnode(&subnode.second, subnode.first, c);
            }
        }
    }
    // Test if this node is a final node
    if (node->isFinal())
    {
        createfinalnode(node, c);
    }

    auto nextnode = label();
    reloc(reloc_nextnode, nextnode);
    pop(R1);
    pop(R3);
}

void Emitter::createfinalnode(Node* node, char c)
{
    auto reloc_nextnode = 0;
    std::string word = node->getWord();
    if (word.size() < 110)
        reloc_nextnode = smallrjmp();
    else
        reloc_nextnode = rjmp();
    int address = _block.size() + _offset;
    for (int n = 0; n <  word.size(); n++)
    {
        _block.push_back(word[n]);
    }
    _block.push_back(0);
    int freq = node->getFrequency();
    char* freqptr = reinterpret_cast<char*>(&freq);
    _block.push_back(freqptr[0]);
    _block.push_back(freqptr[1]);
    _block.push_back(freqptr[2]);
    _block.push_back(freqptr[3]);
    auto nextnode = label();
    if (word.size() < 110)
        smallreloc(reloc_nextnode, nextnode);
    else
        reloc(reloc_nextnode, nextnode);
    mov(R1, reinterpret_cast<void*>(address));
    call(_callstorage);
}

void Emitter::flush()
{
    _offset += _block.size();
    _output.write(_block.raw(), _block.size());
    _block.clear();
}
void Emitter::endsave()
{
    _output.close();
}
void Emitter::beginsave(const std::string& file)
{
    _output.open(file, std::ofstream::out | std::ofstream::binary);   
}
